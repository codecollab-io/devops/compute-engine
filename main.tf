terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.81.0"
    }
  }
}

variable "GCP_SERVICE_ACCOUNT" {
  type = string
}

variable "GCP_SERVICE_KEY" {
  type = string
}

provider "google" {
  project     = "codecollab-io"
  region      = "us-central1"
  credentials = var.GCP_SERVICE_KEY
}

provider "google-beta" {
  project     = "codecollab-io"
  region      = "us-central1"
  credentials = var.GCP_SERVICE_KEY
}

data "terraform_remote_state" "iam" {
  backend = "remote"

  config = {
    organization = "codecollab-io"
    workspaces   = {
      name = "iam-production"
    }
  }
}

#                                  ----------------------------
#              | |                 |     Quality Assurance    |
#              | |===( )   //////  |  AKA I'M BREAKING STUFF  |
#              |_|   |||  | o o|  <----------------------------
#                     ||| ( c  )                  ____
#                      ||| \= /                  ||   \_
#                       ||||||                   ||     |
#                       ||||||                ...||__/|-\"
#                       ||||||             __|________|__
#                         |||             |______________|
#                         |||             || ||      || ||
#                         |||             || ||      || ||
# ------------------------|||-------------||-||------||-||-------
#                         |__>            || ||      || ||
# .================================================================."

data "google_compute_subnetwork" "default_us" {
  region = "us-central1"
  name   = "default"
}

resource "google_compute_instance_template" "cc_compiler_servant_qa" {
  name_prefix = "cc-compiler-servant-qa"
  description = "This template is used to create QA cc-compiler-servant instances."

  labels = {
    env = "qa"
  }

  metadata = {
    env = "qa"
  }

  machine_type         = "e2-micro"
  instance_description = "cc-compiler-servant-qa"

  # Set iptables to allow access to host from container, then start Packer container
  metadata_startup_script = "sudo iptables -A INPUT -i docker0 -j ACCEPT && docker run -d --name packer -v /var/run/docker.sock:/var/run/docker.sock registry.gitlab.com/codecollab-io/compiler/cc-compiler-servant-packer:latest"

  lifecycle {
    create_before_destroy = true
  }

  scheduling {
    preemptible         = true
    automatic_restart   = false
    on_host_maintenance = "TERMINATE"
  }

  disk {
    source_image = "codecollab-io/cc-compiler-servant-test"
    auto_delete  = true
    boot         = true
  }

  network_interface {
    network    = "default"
    subnetwork = data.google_compute_subnetwork.default_us.name
  }

  service_account {
    email  = data.terraform_remote_state.iam.outputs.cc_compiler_servant_account
    scopes = [
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}

resource "google_compute_region_instance_group_manager" "cc_compiler_servant_qa" {
  name               = "cc-compiler-servant-qa"
  base_instance_name = "cc-compiler-servant-qa"

  region = "us-central1"

  version {
    instance_template = google_compute_instance_template.cc_compiler_servant_qa.id
  }
}

resource "google_compute_region_autoscaler" "cc_compiler_servant_qa" {
  name   = "cc-compiler-servant-qa"
  region = "us-central1"
  target = google_compute_region_instance_group_manager.cc_compiler_servant_qa.id

  autoscaling_policy {
    max_replicas = 5
    min_replicas = 1

    cooldown_period = 60

    metric {
      name   = "custom.googleapis.com/instance/slaves/count"
      target = 12
      type   = "GAUGE"
    }
  }
}

#  ____________
# < PRODUCTION >
#  ------------
#         \   ^__^
#          \  (oo)\_______
#             (__)\       )\/\
#                 ||----w |
#  

# $$\   $$\  $$$$$$\  
# $$ |  $$ |$$  __$$\ 
# $$ |  $$ |$$ /  \__|
# $$ |  $$ |\$$$$$$\  
# $$ |  $$ | \____$$\ 
# $$ |  $$ |$$\   $$ |
# \$$$$$$  |\$$$$$$  |
#  \______/  \______/

resource "google_compute_instance_template" "cc_compiler_servant_us" {
  name_prefix = "cc-compiler-servant-us"
  description = "This template is used to create US cc-compiler-servant instances."

  labels = {
    env = "production"
  }

  metadata = {
    env = "production"
  }

  machine_type         = "e2-micro"
  instance_description = "cc-compiler-servant-us"

  # Set iptables to allow access to host from container, then start Packer container
  metadata_startup_script = "sudo iptables -A INPUT -i docker0 -j ACCEPT && docker run -d --name packer -v /var/run/docker.sock:/var/run/docker.sock registry.gitlab.com/codecollab-io/compiler/cc-compiler-servant-packer:latest"

  lifecycle {
    create_before_destroy = true
  }

  scheduling {
    preemptible         = true
    automatic_restart   = false
    on_host_maintenance = "TERMINATE"
  }

  disk {
    source_image = "codecollab-io/cc-compiler-servant-us"
    auto_delete  = true
    boot         = true
  }

  network_interface {
    network    = "default"
    subnetwork = data.google_compute_subnetwork.default_us.name
  }

  service_account {
    email  = data.terraform_remote_state.iam.outputs.cc_compiler_servant_account
    scopes = [
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}

resource "google_compute_region_instance_group_manager" "cc_compiler_servant_us" {
  name               = "cc-compiler-servant-us"
  base_instance_name = "cc-compiler-servant-us"

  region = "us-central1"

  version {
    instance_template = google_compute_instance_template.cc_compiler_servant_us.id
  }
}

resource "google_compute_region_autoscaler" "cc_compiler_servant_us" {
  name   = "cc-compiler-servant-us"
  region = "us-central1"
  target = google_compute_region_instance_group_manager.cc_compiler_servant_us.id

  autoscaling_policy {
    max_replicas = 20
    min_replicas = 1

    cooldown_period = 60

    metric {
      name   = "custom.googleapis.com/instance/slaves/count"
      target = 8
      type   = "GAUGE"
    }
  }
}

#   $$$$$$\   $$$$$$\  
# $$  __$$\ $$  __$$\ 
# $$ /  \__|$$ /  \__|
# \$$$$$$\  $$ |$$$$\ 
#  \____$$\ $$ |\_$$ |
# $$\   $$ |$$ |  $$ |
# \$$$$$$  |\$$$$$$  |
#  \______/  \______/

data "google_compute_subnetwork" "default_sg" {
  region = "asia-southeast1"
  name   = "default"
}

resource "google_compute_instance_template" "cc_compiler_servant_sg" {
  name_prefix = "cc-compiler-servant-sg"
  description = "This template is used to create SG cc-compiler-servant instances."

  labels = {
    env = "production"
  }

  metadata = {
    env = "production"
  }

  machine_type         = "e2-micro"
  instance_description = "cc-compiler-servant-sg"

  # Set iptables to allow access to host from container, then start Packer container
  metadata_startup_script = "sudo iptables -A INPUT -i docker0 -j ACCEPT && docker run -d --name packer -v /var/run/docker.sock:/var/run/docker.sock registry.gitlab.com/codecollab-io/compiler/cc-compiler-servant-packer:latest"

  lifecycle {
    create_before_destroy = true
  }

  scheduling {
    preemptible         = true
    automatic_restart   = false
    on_host_maintenance = "TERMINATE"
  }

  disk {
    source_image = "codecollab-io/cc-compiler-servant-sg"
    auto_delete  = true
    boot         = true
  }

  network_interface {
    network    = "default"
    subnetwork = data.google_compute_subnetwork.default_sg.self_link
  }

  service_account {
    email  = data.terraform_remote_state.iam.outputs.cc_compiler_servant_account
    scopes = [
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}

resource "google_compute_region_instance_group_manager" "cc_compiler_servant_sg" {
  name               = "cc-compiler-servant-sg"
  base_instance_name = "cc-compiler-servant-sg"

  region = "asia-southeast1"

  version {
    instance_template = google_compute_instance_template.cc_compiler_servant_sg.id
  }
}

resource "google_compute_region_autoscaler" "cc_compiler_servant_sg" {
  name   = "cc-compiler-servant-sg"
  region = "asia-southeast1"
  target = google_compute_region_instance_group_manager.cc_compiler_servant_sg.id

  autoscaling_policy {
    max_replicas = 20
    min_replicas = 1

    cooldown_period = 60

    metric {
      name   = "custom.googleapis.com/instance/slaves/count"
      target = 8
      type   = "GAUGE"
    }
  }
}

#  $$$$$$$$\ $$\   $$\ 
# $$  _____|$$ |  $$ |
# $$ |      $$ |  $$ |
# $$$$$\    $$ |  $$ |
# $$  __|   $$ |  $$ |
# $$ |      $$ |  $$ |
# $$$$$$$$\ \$$$$$$  |
# \________| \______/

data "google_compute_subnetwork" "default_eu" {
  region = "europe-west4"
  name   = "default"
}

resource "google_compute_instance_template" "cc_compiler_servant_eu" {
  name_prefix = "cc-compiler-servant-eu"
  description = "This template is used to create EU cc-compiler-servant instances."

  labels = {
    env = "production"
  }

  metadata = {
    env = "production"
  }

  machine_type         = "e2-micro"
  instance_description = "cc-compiler-servant-eu"

  # Set iptables to allow access to host from container, then start Packer container
  metadata_startup_script = "sudo iptables -A INPUT -i docker0 -j ACCEPT && docker run -d --name packer -v /var/run/docker.sock:/var/run/docker.sock registry.gitlab.com/codecollab-io/compiler/cc-compiler-servant-packer:latest"

  lifecycle {
    create_before_destroy = true
  }

  scheduling {
    preemptible         = true
    automatic_restart   = false
    on_host_maintenance = "TERMINATE"
  }

  disk {
    source_image = "codecollab-io/cc-compiler-servant-eu"
    auto_delete  = true
    boot         = true
  }

  network_interface {
    network    = "default"
    subnetwork = data.google_compute_subnetwork.default_eu.self_link
  }

  service_account {
    email  = data.terraform_remote_state.iam.outputs.cc_compiler_servant_account
    scopes = [
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}

resource "google_compute_region_instance_group_manager" "cc_compiler_servant_eu" {
  name               = "cc-compiler-servant-eu"
  base_instance_name = "cc-compiler-servant-eu"

  region = "europe-west4"

  version {
    instance_template = google_compute_instance_template.cc_compiler_servant_eu.id
  }
}

resource "google_compute_region_autoscaler" "cc_compiler_servant_eu" {
  name   = "cc-compiler-servant-eu"
  region = "europe-west4"
  target = google_compute_region_instance_group_manager.cc_compiler_servant_eu.id

  autoscaling_policy {
    max_replicas = 20
    min_replicas = 1

    cooldown_period = 60

    metric {
      name   = "custom.googleapis.com/instance/slaves/count"
      target = 8
      type   = "GAUGE"
    }
  }
}
